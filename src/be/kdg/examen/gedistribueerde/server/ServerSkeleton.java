package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public final class ServerSkeleton {
    private final MessageManager messageManager;
    private final Server server;

    public ServerSkeleton() {
        messageManager = new MessageManager();
        System.out.println("Connect using address:  "+ messageManager.getMyAddress());
        server = new ServerImpl();
    }

    private void sendEmptyReply(MethodCallMessage request){
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    private void handleLog(MethodCallMessage request){
        //haal document uit request
        String text = request.getParameter("text");
        //maak van string document aan
        server.log(new DocumentImpl(text));
        sendEmptyReply(request);
    }

    private void handleCreate(MethodCallMessage request){
        String s = request.getParameter("text");
        Document document = server.create(s);
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("document.text", document.getText());
        messageManager.send(reply, request.getOriginator());
    }

    private void handleToUpper(MethodCallMessage request){
        String ip = request.getParameter("ip");
        int port = Integer.parseInt(request.getParameter("port"));
        String text = request.getParameter("text");
        NetworkAddress address = new NetworkAddress(ip, port);
        Document document = new DocumentStub(address, text);
        server.toUpper(document);

        sendEmptyReply(request);
    }

    private void handleToLower(MethodCallMessage request){
        String ip = request.getParameter("ip");
        int port = Integer.parseInt(request.getParameter("port"));
        String text = request.getParameter("text");
        NetworkAddress address = new NetworkAddress(ip, port);
        Document document = new DocumentStub(address, text);
        server.toLower(document);

        sendEmptyReply(request);

    }

    private void handleType(MethodCallMessage request){
        //call by ref
        String ip = request.getParameter("ip");
        int port = Integer.parseInt(request.getParameter("port"));
        String text = request.getParameter("text");
        NetworkAddress address = new NetworkAddress(ip, port);
        Document document = new DocumentStub(address, "");
        server.type(document, text);

        sendEmptyReply(request);
    }

    private void handleRequests(MethodCallMessage request){
        String methodName = request.getMethodName();
        switch (methodName) {
            case "log":
                handleLog(request);
                break;
            case "create":
                handleCreate(request);
                break;
            case "toUpper":
                handleToUpper(request);
                break;
            case "toLower":
                handleToLower(request);
                break;
            case "type":
                handleType(request);
                break;
            default:
                sendEmptyReply(request);
                break;
        }
    }

    private void run(){
        while(true){
            MethodCallMessage request = messageManager.wReceive();
            handleRequests(request);
        }
    }

    public static void main(String[] args) {
        ServerSkeleton skeleton = new ServerSkeleton();
        skeleton.run();
    }
}
