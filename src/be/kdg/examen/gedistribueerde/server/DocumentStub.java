package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public final class DocumentStub implements Document {
    private final NetworkAddress documentAddress;
    private final MessageManager messageManager;

    public DocumentStub(NetworkAddress documentAddress, String text) {
        this.documentAddress = documentAddress;
        this.messageManager = new MessageManager();
        setText(text);
    }

    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }

    @Override
    public String getText() {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "getText");
        messageManager.send(message, documentAddress);
        MethodCallMessage reply = messageManager.wReceive();
        return reply.getParameter("text");
    }

    @Override
    public void setText(String text) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "setText");
        message.setParameter("text", text);
        messageManager.send(message, documentAddress);
        checkEmptyReply();
    }

    @Override
    public void append(char c) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "append");
        message.setParameter("c", String.valueOf(c));
        messageManager.send(message, documentAddress);
        checkEmptyReply();

    }

    @Override
    public void setChar(int position, char c) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "setChar");
        message.setParameter("position", String.valueOf(position));
        message.setParameter("c", String.valueOf(c));
        messageManager.send(message, documentAddress);
        checkEmptyReply();
    }
}
